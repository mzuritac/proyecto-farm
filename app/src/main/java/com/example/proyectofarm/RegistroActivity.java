package com.example.proyectofarm;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class RegistroActivity extends AppCompatActivity {

    //Definir Objetos
    private EditText TextNombre;
    private EditText TextApellido;
    private EditText TextDni;
    private EditText TextDireccion;
    private EditText TextEmail;
    private EditText TextPassword;
    private EditText TextRePassword;
    private Button BtnFinalizar, BtnCancelar;
    private ProgressDialog progressDialog;

    //Variable de datos a registrar

    private String nombre = "";
    private String apellido = "";
    private String direccion = "";
    private String dni = "";
    private String email = "";
    private String password = "";
    private String repassword = "";

    //Declarando variable firebase

    private FirebaseAuth fbAuth;
    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        //Inicializamos el objetos Firebase
        fbAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();


        //Referenciamos los views
        TextNombre = (EditText) findViewById(R.id.TxtName);
        TextApellido = (EditText) findViewById(R.id.TxtApellido);
        TextDni = (EditText) findViewById(R.id.TxtDni);
        TextDireccion = (EditText) findViewById(R.id.TxtDireccion);
        TextEmail = (EditText) findViewById(R.id.TxtEmail);
        TextPassword = (EditText) findViewById(R.id.TxtPassword);
        TextRePassword = (EditText) findViewById(R.id.TxtRePassword);

        BtnFinalizar = (Button) findViewById(R.id.buttonFinalizar);
        BtnCancelar = (Button) findViewById(R.id.buttonCancelar);

        progressDialog = new ProgressDialog(this);

        //attaching listener to button
        BtnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        BtnFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nombre = TextNombre.getText().toString();
                apellido = TextApellido.getText().toString();
                direccion = TextDireccion.getText().toString();
                email = TextEmail.getText().toString();
                password = TextPassword.getText().toString();
                dni = TextDni.getText().toString();
                repassword = TextRePassword.getText().toString();



                if(!nombre.isEmpty() && !apellido.isEmpty() && !dni.isEmpty() && !direccion.isEmpty() && !email.isEmpty() && !password.isEmpty()){
                    if(password.length()>= 6){
                        if(password.equals(repassword)){
                            progressDialog.setMessage("Realizando registro en linea...");
                            progressDialog.show();
                            registrarUsuario();
                        }else{
                            Toast.makeText(RegistroActivity.this,"Las contraseñas no coinciden ",Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(RegistroActivity.this,"La contraseña debe tener al menos 6 caracteres",Toast.LENGTH_LONG).show();
                    }

                }else{
                    Toast.makeText(RegistroActivity.this,"Llene todos los campos",Toast.LENGTH_LONG).show();
                }

            }
        });
        //BtnCancelar.setOnClickListener(this);
    }
    private void registrarUsuario(){
        fbAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){

                    Map<String, Object> map = new HashMap<>();
                    map.put("nombre",nombre);
                    map.put("apellido",apellido);
                    map.put("direccion",direccion);
                    map.put("dni",dni);
                    map.put("email",email);
                    map.put("password",password);




                    String id = fbAuth.getCurrentUser().getUid();

                    mDatabase.child("Users").child(id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task2) {
                            if (task2.isSuccessful()){
                                Toast.makeText(RegistroActivity.this,"Usuario creado",Toast.LENGTH_LONG).show();
                                startActivity(new Intent(RegistroActivity.this,FirstMenuActivity.class));
                                finish();
                                progressDialog.dismiss();

                            }
                        }
                    });

                }else{
                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                        Toast.makeText(RegistroActivity.this, "Email de usuario ya existe", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(RegistroActivity.this, "No se pudo registrar el usuario", Toast.LENGTH_LONG).show();
                    }
                    progressDialog.dismiss();
                }

            }
        });
    }
}
