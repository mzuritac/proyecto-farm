package com.example.proyectofarm;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MenuActivity extends AppCompatActivity {

    private Button BtnCerrar;
    private Button BtnConsultar;
    private Button bInkafarma;
    private Button bMifarma;
    private Button bPacifico;
    private FirebaseAuth fbAuth;
    private TextView TextBienvenido;
    private FirebaseDatabase mDatabase;
    private DatabaseReference mGetReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        TextBienvenido = (TextView) findViewById(R.id.txtBienvenido);
        BtnCerrar = (Button) findViewById(R.id.btnCSesion);
        BtnConsultar = (Button) findViewById(R.id.btnConsultar);
        bInkafarma = (Button) findViewById(R.id.btnInkafarma);
        bMifarma = (Button) findViewById(R.id.btnMifarma);
        bPacifico = (Button) findViewById(R.id.btnPacifico);
        fbAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mGetReference = mDatabase.getReference();

        String id = fbAuth.getCurrentUser().getUid();

        mGetReference.child("Users").child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String nombre = dataSnapshot.child("nombre").getValue().toString();
                    String apellido = dataSnapshot.child("apellido").getValue().toString();
                    TextBienvenido.setText("Bienvenido: " + nombre + " " + apellido);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        bInkafarma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("http://www.inkafarma.pe/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        bMifarma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.mifarma.com.pe");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        bPacifico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.parafarmaciadelpacifico.com");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        BtnConsultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        BtnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(MenuActivity.this, MainActivity.class));
                finish();
            }
        });
    }
}
