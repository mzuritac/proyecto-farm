package com.example.proyectofarm;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class MainActivity extends AppCompatActivity {

    //Definiendo Objetos
    private EditText TextEmail;
    private EditText TextPassword;
    private Button btnRegistrar, btnLogin;
    private ProgressDialog progressDialog;

    //Declarando instancia Firebase


    private FirebaseAuth fbAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inicializamos el objetos Firebase
        fbAuth = FirebaseAuth.getInstance();

        //Referenciamos los views
        TextEmail = (EditText) findViewById(R.id.TxtEmail);
        TextPassword = (EditText) findViewById(R.id.TxtPassword);



        btnRegistrar = (Button) findViewById(R.id.BotonRegistrar);
        btnLogin = (Button) findViewById(R.id.BotonEntrar);

        progressDialog = new ProgressDialog(this);

        //attaching listener to button
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*startActivity(new Intent(MainActivity.this,RegistroActivity.class));
                finish();*/
                Intent intent = new Intent (v.getContext(), RegistroActivity.class);
                startActivityForResult(intent, 0);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               loguearUsuario();
            }
        });


    }

   // private void registrarUsuario(){
        //Obtenemos el email y contraseña desde las cajas de texto
        /*String email = TextEmail.getText().toString().trim();
        String password = TextPassword.getText().toString().trim();

        //Verificamos que las cajas de texto no esten vacias
        if(TextUtils.isEmpty(email)){
            Toast.makeText(this,"Se debe ingresar un email",Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(password)){
            Toast.makeText(this,"Se debe ingresar una contraseña",Toast.LENGTH_LONG).show();
            return;
        }

        progressDialog.setMessage("Realizando registro en linea...");
        progressDialog.show();

        //Creando nuevo usuario
        fbAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete (@NonNull Task < AuthResult > task) {
                //Verificar
                if (task.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Se ha registrado el usuario con el email "+ TextEmail.getText(), Toast.LENGTH_LONG).show();
                    //TextEmail.setText("");
                    //TextPassword.setText("");
                    //findViewById(R.id.TxtEmail).requestFocus();
                } else {
                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                        Toast.makeText(MainActivity.this, "Email de usuario ya existe", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(MainActivity.this, "No se pudo registrar el usuario", Toast.LENGTH_LONG).show();
                    }
                }
                progressDialog.dismiss();
            }
        });*/
    //}
    protected void onStart(){
        super.onStart();
        FirebaseUser currentUser = fbAuth.getCurrentUser();
        updateUI(currentUser);

    }
    private void updateUI(FirebaseUser user){
        if(user != null){
            startActivity(new Intent(MainActivity.this,FirstMenuActivity.class));
            finish();
        }
    }
    private void loguearUsuario(){
        //Obtenemos el email y contraseña desde las cajas de texto
        String email = TextEmail.getText().toString().trim();
        String password = TextPassword.getText().toString().trim();

        //Verificamos que las cajas de texto no esten vacias
        if(TextUtils.isEmpty(email)){
            Toast.makeText(this,"Se debe ingresar un email",Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(password)){
            Toast.makeText(this,"Se debe ingresar una contraseña",Toast.LENGTH_LONG).show();
            return;
        }

        progressDialog.setMessage("Validando Informacion...");
        progressDialog.show();

        //Creando nuevo usuario
        fbAuth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete (@NonNull Task < AuthResult > task) {
                        //Verificar
                        if (task.isSuccessful()) {
                            Toast.makeText(MainActivity.this, "Bienvenido", Toast.LENGTH_LONG).show();
                            FirebaseUser user = fbAuth.getCurrentUser();
                            updateUI(user);

                        } else {
                            Toast.makeText(MainActivity.this, "No estas registrado", Toast.LENGTH_LONG).show();
                            updateUI(null);
                        }
                        progressDialog.dismiss();
                    }
                });
    }

}
